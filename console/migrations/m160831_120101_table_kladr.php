<?php

use yii\db\Migration;

class m160831_120101_table_kladr extends Migration
{
    public function up()
    {
        $this->execute(file_get_contents(__DIR__ . '/kladrForSend.sql'));
    }

    public function down()
    {
        $this->dropTable('kladr');
        $this->dropTable('socrbase');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
