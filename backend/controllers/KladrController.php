<?php

namespace backend\controllers;

use common\models\Kladr;

class KladrController extends \yii\web\Controller
{
	/**
     * Lists Regions
     * @return JSON
     */
    public function actionRegions()
    {
    	//if ( Yii::$app->request->isAjax && $app->request->post('query') ) {
    	if ( $query = \Yii::$app->request->get('query') ) {
			\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    		if (mb_strlen($query) < 2 ) {
    		 	return ["items" => []];
    		}

            $items = Kladr::getRegions($query);
            return ["items" => $items];
        }

        header("Location: /");
        exit;
    }

    /**
     * Lists Points
     * @return JSON
     */
    public function actionPoints()
    {
    	//if ( Yii::$app->request->isAjax && $app->request->post('query') ) {
    	if ( \Yii::$app->request->get('query') && \Yii::$app->request->get('code') ) {
             \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
             $items = Kladr::getPointsByRegionCode(\Yii::$app->request->get('code'), \Yii::$app->request->get('query'));
             return ["items" => $items];
        }

        header("Location: /");
        exit;
    }

}