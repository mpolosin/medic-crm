<?php

use yii\db\Migration;
use yii\base\Security;


class m160902_130948_add_administrator extends Migration
{
    public function generatePassword(){

    }

    public function up()
    {
        $this->insert('{{%user}}', [
           'id' => 1,
           'username' => 'admin',
           'auth_key' => '9TEUQ1ShMId4PIfhlNlmd8QXuVShgWf3',
           'password_hash'  => '$2y$13$0KUZunb3wsNmeM6cwg7xxOO2U1PsqQpnbuJmAtfpsYaW6dc7bk3D.',
           'password_reset_token' => NULL,
           'email'    => 'example@gmail.com',
           'status'   => 10,
           'role'     => 10,
            'created_at' => time(),
            'updated_at' => time(),
        ]);
    }

    public function down()
    {
        $this->delete('{{%user}}', [
            'username' => 'admin',
        ]);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
