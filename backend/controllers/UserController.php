<?php

namespace backend\controllers;

use Yii;
use common\models\User;
use common\models\Profile;
use common\models\UserSearch;
use yii\base\ErrorException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\controllers\Kladr;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();
        $profile = new Profile();
        $model->setPassword($model->password);
        $model->generateAuthKey();

        if ($model->load(Yii::$app->request->post())) {
            if($model->validate()){
                if($model->save()){
                    $profile->user_id = $model->id;
                    $profile->save();
                }
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'profile' => $profile,
            ]);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $profile = $this->findProfile($id);

        if ($model->load(Yii::$app->request->post())) {
            if($model->validate()){
                $listStatus = $_POST['User']['status'];
                /*if(!empty($listStatus)) {
                    foreach ($listStatus as $value) {
                        $model->status = $value;
                    }
                }*/
                $model->save();
                //$profile->save();
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'profile' => $profile,
            ]);
        }
    }

    public function actionKladr(){
        // Инициализация api, в качестве параметров указываем токен и ключ для доступа к сервису
        $api = new Api('51dfe5d42fb2b43e3300006e', '86a2c2a06f1b2451a87d05512cc2c3edfdf41969');

        // Формирование запроса
        $query = new Kladr\Query();
        $query->ContentName = 'Арх';
        $query->ContentType = Kladr\ObjectType::City;
        $query->WithParent = true;
        $query->Limit = 2;

        // Получение данных в виде ассоциативного массива
        $arResult = $api->QueryToArray($query);
        var_dump($arResult); die();
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Страница пользователя не найдена.');
        }
    }
    protected function findProfile($id)
    {
        if($model = Profile::find()->where(['user_id' => $id])->one()) :
            if (($model) !== null) {
                return $model;
            }else {
                throw new NotFoundHttpException('Данные профиля пользователя не найдены.');
            }
        endif;
    }
}
