<?php
namespace common\models;

use yii\db\ActiveRecord;
use Yii;

class Kladr extends ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%kladr}}';
    }

    public static function getRegions($query){
        $regions = Yii::$app->db->createCommand("SELECT code,name FROM ". self::tableName()." WHERE code LIKE :like AND name LIKE :query")
           ->bindValue(':like', "%00000000000")
           ->bindValue(':query', $query."%")
           ->queryAll();

        return $regions; 
    }

    public static function getPointsByRegionCode($codeKladr, $query){
        $points = [];
        $allItems = self::getAllFromRegion($codeKladr, $query);

        foreach ($allItems as $item) {    

            $points[] = [
                'name' => $item["name"],
                'fullName' => self::getFullName($item["code"], false, true, true, true)
            ];
        }

        return $points; 
    }

    public static function getAllFromRegion($codeKladr, $query) {
        return Yii::$app->db->createCommand("SELECT code,name FROM ". self::tableName()." WHERE code LIKE :like AND name LIKE :query")
           ->bindValue(':like', self::substr($codeKladr,0,2) . "%")
           ->bindValue(':query', $query."%")
           ->queryAll();
    }

    public static function getFullName($codeKladr, $showRegion = true, $showDistrict = true, $showCity = true, $showCommunity = true) {
        $codeData = self::getCodeData($codeKladr);

        $fullName = "";

        if ( $showDistrict && $codeData["district"] ) {
            $fullName .= ", ".self::getNameByCode($codeData["district"]);
        }
        if ( $showCity && $codeData["city"] ) {
            $fullName .= ", ".self::getNameByCode($codeData["city"]);
        }
        if ( $showCommunity && $codeData["community"] ) {
            $fullName .= ", ".self::getNameByCode($codeData["community"]);
        }

        return $fullName;
    }

    public static function getNameByCode($codeKladr) {
        $data = Yii::$app->db->createCommand("SELECT name, socr FROM ". self::tableName()." WHERE code=:code")
           ->bindValue(':code', $codeKladr)
           ->queryOne();

        return $data['socr'] .". ".$data['name'];
          
    }

    public static function getCodeData($codeKladr) {
        $region = substr($codeKladr,0,2);
        $data = [
            'region' => $region."00000000000",
            'district' => false,
            'city' => false,
            'community' => false,
        ];

        if ( $district = self::getShortCode($codeKladr,2,3) ) {
            $data['district'] = $region.$district."00000000";
        }
        else {
            $district = "000";
        }

        if ( $city = self::getShortCode($codeKladr,5,5) ) {
            $data['city'] = $region.$district.$city."000";
        }        

        if ( $community = self::getShortCode($codeKladr,10,3) ) {
            $data['community'] = $codeKladr;
        }

        return  $data;
    }

    public static function getShortCode($codeKladr, $start, $length) {
        if ( intval(substr($codeKladr,$start,$length)) )
            return substr($codeKladr,$start,$length);
        else
            return false;
    }

}
?>