<?php

use yii\db\Migration;

class m160822_135602_table_profile extends Migration
{
    public function up()
    {

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%profile}}', [
            'id' => $this->primaryKey(),
            'firstname' => $this->string()->notNull(),
            'secondname' => $this->string()->notNull(),
            'thirdname' => $this->string()->notNull(),
            'region' => $this->string()->notNull(),
            'city' => $this->string()->notNull(),
            'institution' => $this->string()->notNull(),
            'phonenumber' => $this->string()->notNull(),
            'user_id' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addForeignKey('userid_fr_key', '{{%profile}}', 'user_id', '{{%user}}', 'id');

    }

    public function down()
    {
        $this->dropForeignKey('userid_fr_key', '{{%profile}}');
        $this->dropTable('{{%profile}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
