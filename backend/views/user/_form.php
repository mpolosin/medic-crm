<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>


    <?= $form->field($model, 'username')->textInput() ?>

    <?= $form->field($model, 'email')->textInput() ?>

    <?php if(Yii::$app->controller->route == 'user/create') : ?>
        <?= $form->field($model, 'password')->passwordInput() ?>
    <?php endif; ?>

    <?= $form->field($model, 'role')->dropDownList(ArrayHelper::map($model->roleArray(), 'id', 'name')) ?>

    <?= $form->field($model, 'status')->dropDownList(ArrayHelper::map($model->statusArray(), 'id', 'name') )->label('Заблокировать') ?>

    <?php if(!empty($profile)) : ?>
        <?php if(Yii::$app->controller->route == 'user/update') : ?>
            <h2>Регистрационные данные врача</h2>

            <?= $form->field($profile, 'secondname')->textInput() ?>

            <?= $form->field($profile, 'firstname')->textInput() ?>

            <?= $form->field($profile, 'thirdname')->textInput() ?>

            <?= $form->field($profile, 'region')->textInput() ?>

            <?= $form->field($profile, 'city')->textInput() ?>

            <?= $form->field($profile, 'institution')->textInput() ?>

            <?= $form->field($profile, 'phonenumber')->textInput() ?>

        <?php endif; ?>
    <?php endif; ?>

    <?php if(Yii::$app->controller->route == 'user/create') :
        Pjax::begin(['enablePushState' => false]) ?>
        <?= Html::a('Добавить форму врача', Url::toRoute(['profile/create']), ['class' => 'btn btn-success', 'style' => 'margin-bottom: 15px'] ) ?>
        <?php Pjax::end();
    endif; ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
