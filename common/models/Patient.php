<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%patient}}".
 *
 * @property integer $id
 * @property string $firstname
 * @property string $secondname
 * @property string $thirdname
 * @property string $phonenumber
 * @property string $email
 * @property integer $gender
 * @property integer $birthday
 * @property integer $age
 * @property string $city
 * @property string $position
 * @property integer $group_bld
 * @property integer $rz_faktor
 * @property integer $weight
 * @property integer $height
 * @property integer $mass_body
 * @property integer $bad_habits
 * @property string $diagnos
 * @property integer $preparation_enter_id
 * @property integer $preparation_id
 * @property integer $parent_id
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property User $parent
 */
class Patient extends \yii\db\ActiveRecord
{
    public $fullname;

    const GENDER_MALE = 1;
    const GENDER_FEMALE = 2;

    const GROUP_BLD_I = 1;
    const GROUP_BLD_II = 2;
    const GROUP_BLD_III = 3;
    const GROUP_BLD_IV = 4;

    const BAD_HABIT_SM = 1;
    const BAD_HABIT_DR = 2;
    const BAD_HABIT_DL = 3;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%patient}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['firstname', 'secondname', 'thirdname', 'phonenumber', 'email', 'gender', 'birthday', 'age', 'city', 'position', 'group_bld', 'rz_faktor', 'weight', 'height', 'mass_body', 'bad_habits', 'diagnos'], 'required', 'message' => 'Заполните пожалуйста поле'],
            [['id', 'gender', 'birthday', 'age', 'group_bld', 'rz_faktor', 'weight', 'height', 'mass_body', 'bad_habits', 'preparation_enter_id', 'preparation_id', 'parent_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['diagnos'], 'string'],
            [['firstname', 'secondname', 'thirdname', 'phonenumber', 'email', 'city', 'position'], 'string', 'max' => 255],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['parent_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'firstname' => 'Имя',
            'secondname' => 'Фамилия',
            'thirdname' => 'Отчество',
            'phonenumber' => 'Телефонынй номер',
            'email' => 'Email',
            'gender' => 'Пол',
            'birthday' => 'Дата рождения',
            'age' => 'Возраст',
            'city' => 'Город',
            'position' => 'Должность',
            'group_bld' => 'Группа крови',
            'rz_faktor' => 'Резус. фактор',
            'weight' => 'Вес',
            'height' => 'Рост',
            'mass_body' => 'Индекс массы тела',
            'bad_habits' => 'Вредные привычки',
            'diagnos' => 'Диагноз',
            'preparation_enter_id' => 'Принимаемые препараты',
            'preparation_id' => 'Препараты',
            'parent_id' => 'Создан врачом',
            'status' => 'Статус',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата редактирования',
        ];
    }

    public function arrGender(){
        return [
            ['id' => self::GENDER_MALE, 'name' => 'Мужчина'],
            ['id' => self::GENDER_FEMALE, 'name' => 'Женщина'],
        ];
    }

    public function arrBroupBld(){
        return [
            ['id' => self::GROUP_BLD_I, 'name' => 'I'],
            ['id' => self::GROUP_BLD_II, 'name' => 'II'],
            ['id' => self::GROUP_BLD_III, 'name' => 'III'],
            ['id' => self::GROUP_BLD_IV, 'name' => 'IV'],
        ];
    }

    public function arrBadHabit(){
        return [
            ['id' => self::BAD_HABIT_SM, 'name' => 'Курение'],
            ['id' => self::BAD_HABIT_DR, 'name' => 'Алкоголь'],
            ['id' => self::BAD_HABIT_DL, 'name' => 'Наркотики'],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'parent_id']);
    }
}
