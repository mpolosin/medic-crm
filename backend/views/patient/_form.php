<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Patient */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="patient-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'firstname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'secondname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'thirdname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phonenumber')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'gender')->dropDownList(ArrayHelper::map($model->arrGender(), 'id', 'name')) ?>

    <label>День рождения</label>
    <?= DatePicker::widget([
        'model'     => $model,
        'attribute' => 'birthday',
        'language'  => 'ru',
        'dateFormat' => 'yyyy-MM-dd',
        'value'     => date('Y-m-d', time()),
        'options'   => [
            'class' => 'form-control',
        ],
    ]) ?><span class="glyphicon date"></span>

    <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'position')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'group_bld')->dropDownList(ArrayHelper::map($model->arrBroupBld(), 'id', 'name')) ?>

    <?= $form->field($model, 'rz_faktor')->textInput() ?>

    <?= $form->field($model, 'weight')->textInput()->label('Вес (кг)') ?>

    <?= $form->field($model, 'height')->textInput()->label('Рост (см)') ?>

    <?= $form->field($model, 'mass_body')->textInput() ?>

    <?= $form->field($model, 'bad_habits')->dropDownList(ArrayHelper::map($model->arrBadHabit(), 'id', 'name'))?>

    <?= $form->field($model, 'diagnos')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
