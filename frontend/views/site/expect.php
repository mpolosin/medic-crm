<?php

/* @var $this yii\web\View */

$this->title = 'Ожидание модерации';
?>
<div class="site-index">

    <div class="body-content">

        <div class="row">
            <div class="col-md-12">
                <h2>Спасибо за регистрацию! Ожидайте пожалуйста модерации администратором.</h2>
            </div>
        </div>

    </div>
</div>
