<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin([
        'action' => Url::toRoute(['profile/update', 'userId' => $model->id]),
        'options' => [
            'data-pjax' => '1',
        ],
        'id' => 'profileUpdateForm',
    ]); ?>


    <?php foreach ($model->profile as $key => $profile): ?>
        <?= $form->field($profile, "[$key]firstname") ?>
        <?= $form->field($profile, "[$key]secondname") ?>
    <?php endforeach ?>

    <?= Html::a('Добавить форму профиля', Url::toRoute(['profile/create', 'userId' => $model->id]), ['class' => 'btn btn-success add-form-profile']); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
