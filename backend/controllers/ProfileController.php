<?php

namespace backend\controllers;

use Yii;
use common\models\User;
use common\models\Profile;
use yii\base\Model;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/*
 * Profile controller
 */

class ProfileController extends Controller{

    protected function batchUpdate($items){
        if(Model::loadMultiple($items, Yii::$app->request->post()) && Model::validateMultiple($items)){
            foreach($items as $key => $item){
                $item->save();
            }
        }
    }

    /**
     * Finds the Profile model
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Profile the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id){
        if(($model = Profile::findOne($id)) !== null ){
            return $model;
        }else{
            throw new NotFoundHttpException('The request page does not exist.');
        }
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findUser($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionUpdate($userId){
        $user = $this->findUser($userId);
        $this->batchUpdate($user->profile);
        return $this->renderAjax('_profile', ['model' => $user]);
    }

    public function actionCreate(){
        //$user = $this->findUser($userId);
        $model = new Profile;
        $model->addOne();
        //$user->link('profile', $model); // link сохраняет в БД без валидации
        return $this->renderAjax('_profile', ['model' => $model]);
    }

   /* public function actionDelete($id){
        $model = $this->findModel($id);
        $user = $model->user;
        $model->delete();
        return $this->renderAjax('_profile');
    }*/
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->renderAjax('_profile');
    }

    /**
     * @inheritdoc
     */
    public function behaviors(){
        return [
            'access' => [
              'class' => AccessControl::className(),
              'rules' => [
                  [
                    'actions' => ['create', 'update', 'delete'],
                    'allow'  => true,
                    'roles'   => ['@'],
                  ],
              ]
            ],
        ];
    }

}