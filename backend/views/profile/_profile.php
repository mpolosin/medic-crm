<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <h2>Форма регистрационных данных врача</h2>
    <div class="form-group">
        <?= Html::activeLabel($model, 'secondname') ?>
        <?= Html::activeInput('text', $model, "secondname", ['class' => 'form-control']) ?>
    </div>
    <div class="form-group">
        <?= Html::activeLabel($model, 'firstname') ?>
        <?= Html::activeInput('text', $model, "firstname", ['class' => 'form-control']) ?>
    </div>
    <div class="form-group">
        <?= Html::activeLabel($model, 'thirdname') ?>
        <?= Html::activeInput('text', $model, "thirdname", ['class' => 'form-control']) ?>
    </div>
    <div class="form-group">
        <?= Html::activeLabel($model, 'city') ?>
        <?= Html::activeInput('text', $model, "city", ['class' => 'form-control']) ?>
    </div>
    <div class="form-group">
        <?= Html::activeLabel($model, 'region') ?>
        <?= Html::activeInput('text', $model, "region", ['class' => 'form-control']) ?>
    </div>
    <div class="form-group">
        <?= Html::activeLabel($model, 'institution') ?>
        <?= Html::activeInput('text', $model, "institution", ['class' => 'form-control']) ?>
    </div>
    <div class="form-group">
        <?= Html::activeLabel($model, 'phonenumber') ?>
        <?= Html::activeInput('text', $model, "phonenumber", ['class' => 'form-control']) ?>
    </div>

    <div class="form-group">
        <?= Html::a('Удалить форму', Url::toRoute(['profile/delete']), [ 'class' => 'btn btn-danger']) ?>
    </div>

</div>
