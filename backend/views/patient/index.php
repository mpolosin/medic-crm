<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\PatientSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Пациенты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <p>
        <?= Html::a('Добавить нового пациента', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'fullname',
                'label'     => 'ФИО',
                'content'   => function($model){
                    return $model->firstname . " " . $model->secondname . " " . $model->thirdname;
                }
            ],
            'phonenumber',
            [
                'attribute' => 'birthday',
                'format'    => 'date',
                'label'     => 'Дата рождения',
                'content'   => function($model){
                    return $model->birthday . " (" . $model->age .") ";
                }
            ],
            'city',
            'group_bld',
            'rz_faktor',
            'weight',
            'height',
            [
                'attribute' => 'user.username',
                'label'     => 'Создан'
            ],
            'created_at:date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?></div>
