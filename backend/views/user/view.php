<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = 'Просмотр пользователя: ' . $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы точно хотите удалить пользователя?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'formatter'  => [
            'class'  => 'yii\i18n\Formatter',
            'dateFormat' => 'php:d.m.Y',
            'locale' => 'ru',
        ],
        'attributes' => [
            'username',
            'email:email',
            [
                'label' => 'Роль пользователя',
                'value' => $model->attrStrRole($model),
            ],
            [
                'label' => 'Статус',
                'value' => $model->attrStrStatus($model),
            ],
            'created_at:date',
            'updated_at:date',
        ],
    ]); ?>

</div>
