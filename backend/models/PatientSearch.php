<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Patient;

/**
 * PatientSearch represents the model behind the search form about `common\models\Patient`.
 */
class PatientSearch extends Patient
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'gender', 'birthday', 'age', 'group_bld', 'rz_faktor', 'weight', 'height', 'mass_body', 'bad_habits', 'preparation_enter_id', 'preparation_id', 'parent_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['firstname', 'secondname', 'thirdname', 'phonenumber', 'email', 'city', 'position', 'diagnos'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Patient::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'gender' => $this->gender,
            'birthday' => $this->birthday,
            'age' => $this->age,
            'group_bld' => $this->group_bld,
            'rz_faktor' => $this->rz_faktor,
            'weight' => $this->weight,
            'height' => $this->height,
            'mass_body' => $this->mass_body,
            'bad_habits' => $this->bad_habits,
            'preparation_enter_id' => $this->preparation_enter_id,
            'preparation_id' => $this->preparation_id,
            'parent_id' => $this->parent_id,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'firstname', $this->firstname])
            ->andFilterWhere(['like', 'secondname', $this->secondname])
            ->andFilterWhere(['like', 'thirdname', $this->thirdname])
            ->andFilterWhere(['like', 'phonenumber', $this->phonenumber])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'position', $this->position])
            ->andFilterWhere(['like', 'diagnos', $this->diagnos]);

        return $dataProvider;
    }
}
