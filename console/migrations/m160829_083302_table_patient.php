<?php

use yii\db\Migration;

class m160829_083302_table_patient extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if($this->db->driverName === 'mysql'){
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%patient}}', [
            'id' => $this->primaryKey(),
            'firstname'   => $this->string()->notNull(),
            'secondname'  => $this->string()->notNull(),
            'thirdname'   => $this->string()->notNull(),
            'phonenumber' => $this->string()->notNull(),
            'email'       => $this->string()->notNull(),
            'gender'      => $this->smallInteger(2)->notNull(),
            'birthday'    => $this->integer()->notNull(),
            'age'         => $this->integer()->notNull(),
            'city'        => $this->string()->notNull(),
            'position'    => $this->string()->notNull(),
            'group_bld'   => $this->smallInteger(2)->notNull(),
            'rz_faktor'   => $this->smallInteger(2)->notNull(),
            'weight'      => $this->integer()->notNull(),
            'height'      => $this->integer()->notNull(),
            'mass_body'   => $this->integer()->notNull(),
            'bad_habits'  => $this->integer()->notNull(),
            'diagnos'     => $this->text()->notNull(),
            'preparation_enter_id' => $this->integer()->notNull(),
            'preparation_id' => $this->integer()->notNull(),
            'parent_id'   => $this->integer()->notNull(),
            'status'      => $this->smallInteger()->notNull()->defaultValue(5),
            'created_at'  => $this->integer()->notNull(),
            'updated_at'  => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addForeignKey('fk_parent_id', '{{%patient}}', 'parent_id', '{{%user}}', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('fk_parent_id', '{{%patient}}');
        $this->dropTable('{{%patient}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
