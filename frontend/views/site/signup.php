<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Регистрация';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Пожалуйста, заполните поля для регистрации:</p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

                <?= $form->field($model, 'secondname')->textInput()->label('Фамилия') ?>

                <?= $form->field($model, 'firstname')->textInput()->label('Имя') ?>

                <?= $form->field($model, 'thirdname')->textInput()->label('Отчество') ?>

                <?= $form->field($model, 'region')->textInput()->label('Регион') ?>

                <?= $form->field($model, 'city')->textInput()->label('Город') ?>

                <?= $form->field($model, 'institution')->textInput()->label('Мед. учреждение') ?>

                <?= $form->field($model, 'phonenumber')->textInput()->label('Контактный телефон') ?>

                <?= $form->field($model, 'username')->textInput()->label('Логин') ?>

                <?= $form->field($model, 'email')->textInput()->label('Email') ?>

                <?= $form->field($model, 'password')->passwordInput() ?>

                <div class="form-group">
                    <?= Html::submitButton('Зарегистрироваться', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
