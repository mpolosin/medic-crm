<?php
namespace frontend\models;

use yii\base\Model;
use common\models\User;
use common\models\Profile;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $firstname;
    public $secondname;
    public $thirdname;
    public $region;
    public $city;
    public $institution;
    public $phonenumber;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],

            [['firstname', 'secondname', 'thirdname', 'institution', 'phonenumber'], 'required'],
            [['firstname', 'secondname', 'thirdname', 'institution', 'region', 'city'], 'string'],
            [['firstname', 'secondname', 'thirdname'], 'string', 'max' => 255],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $user = new User();
        $profile = new Profile();
        $user->username = $this->username;
        $user->email = $this->email;
        $profile->firstname = $this->firstname;
        $profile->secondname = $this->secondname;
        $profile->thirdname = $this->thirdname;
        $profile->region = $this->region;
        $profile->city = $this->city;
        $profile->institution = $this->institution;
        $profile->phonenumber = $this->phonenumber;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        
        //return $user->save() ? $user : null;

        if($user->save()){
            $profile->user_id = $user->id;
            $profile->save();
        }else{
            return null;
        }
    }

    public function attributeLabels()
    {
        return [
            'username'      => 'Имя пользователя',
            'email'         => 'Email',
            'password'      => 'Пароль',
            'firstname'     => 'Имя',
            'secondname'    => 'Фамилия',
            'thirdname'     => 'Отчество',
            'region'        => 'Регион',
            'city'          => 'Город',
            'institution'   => 'Мед. учреждение',
            'phonenumber'   => 'Телефонный номер',
        ];
    }
}
