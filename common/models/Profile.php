<?php

namespace common\models;

use Yii;
use common\models\User;

/**
 * This is the model class for table "{{%profile}}".
 *
 * @property integer $id
 * @property string $firstname
 * @property string $secondname
 * @property string $thirdname
 * @property string $region
 * @property string $city
 * @property string $institution
 * @property string $phonenumber
 * @property integer $user_id
 *
 * @property User $user
 */
class Profile extends \yii\db\ActiveRecord
{
    const DEFAULT_FIRSTNAME = 'Имя врача';
    const DEFAULT_SECONDNAME = 'Фамилия врача';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%profile}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'integer'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['firstname', 'secondname', 'thirdname', 'city', 'region', 'institution', 'phonenumber'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'firstname' => 'Имя',
            'secondname' => 'Фамилия',
            'thirdname' => 'Отчество',
            'region' => 'Регион',
            'city' => 'Город',
            'institution' => 'Мед. учреждение',
            'phonenumber' => 'Телефон',
        ];
    }

    public function addOne(){
        $this->firstname = self::DEFAULT_FIRSTNAME;
        $this->secondname = self::DEFAULT_SECONDNAME;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
